{
  description = "virtual environments";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    devshell.url = "github:numtide/devshell";
    flake-utils.url = "github:numtide/flake-utils";
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    naersk = {
      url = github:nix-community/naersk;
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # TODO: fission cli via nix
    # fission = {
    #   url = "file+https://github.com/fission-suite/fission/releases/download/$FISSION_VERSION/fission-cli-ubuntu-20.04-x86_64";
    #   flake = false;
    # };
  };

  outputs = { self, flake-utils, devshell, nixpkgs, fenix, naersk /* , fission */ }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ devshell.overlay ];

        # From: https://github.com/nix-community/naersk/blob/master/examples/static-musl/flake.nix
        # pkgs = nixpkgs.legacyPackages.${system};
        pkgs = import nixpkgs {
          inherit system overlays;
        };
        toolchain = with fenix.packages.${system}; combine [
          minimal.rustc
          minimal.cargo
          targets.x86_64-unknown-linux-musl.latest.rust-std
        ];
        naersk-lib = naersk.lib.${system}.override {
          cargo = toolchain;
          rustc = toolchain;
        };
      in
      rec {

        # From: https://github.com/nix-community/naersk/blob/master/examples/static-musl/flake.nix
        packages = rec {
          x86_64-unknown-linux-musl = naersk-lib.buildPackage {
            # The rust compiler is internally a cross compiler, so a single
            # toolchain can be used to compile multiple targets. In a hermetic
            # build system like nix flakes, there's effectively one package for
            # every permutation of the supported hosts and targets.
            # i.e.: nix build .#packages.x86_64-linux.x86_64-pc-windows-gnu
            # where x86_64-linux is the host and x86_64-pc-windows-gnu is the target
            root = ./.;
            nativeBuildInputs = with pkgs; [ pkgsStatic.stdenv.cc ];

            # Configures the target which will be built.
            # ref: https://doc.rust-lang.org/cargo/reference/config.html#buildtarget
            CARGO_BUILD_TARGET = "x86_64-unknown-linux-musl";

            # Enables static compilation.
            #
            # If the resulting executable is still considered dynamically
            # linked by ldd but doesn't have anything actually linked to it,
            # don't worry. It's still statically linked. It just has static
            # position independent execution enabled.
            # ref: https://github.com/rust-lang/rust/issues/79624#issuecomment-737415388
            CARGO_BUILD_RUSTFLAGS = "-C target-feature=+crt-static";

            # Configures the linker which will be used. cc.targetPrefix is
            # sometimes different than the targets used by rust. i.e.: the
            # mingw-w64 linker is "x86_64-w64-mingw32-gcc" whereas the rust
            # target is "x86_64-pc-windows-gnu".
            #
            # This is only necessary if rustc doesn't already know the correct linker to use.
            #
            # ref: https://doc.rust-lang.org/cargo/reference/config.html#targettriplelinker
            # CARGO_TARGET_X86_64_UNKNOWN_LINUX_MUSL_LINKER = with pkgs.pkgsStatic.stdenv;
            #   "${cc}/bin/${cc.targetPrefix}gcc";

            doCheck = true;
          };
        };

        devShell = pkgs.mkShell rec {
          buildInputs = with pkgs; [
            llvmPackages_latest.llvm
            llvmPackages_latest.bintools
            zlib.out
            rustup
            # xorriso
            grub2
            # qemu
            llvmPackages_latest.lld
            # python3

            trunk
            cargo-edit
            nixpkgs-fmt
          ];
          RUSTC_VERSION = "nightly";
          # https://github.com/rust-lang/rust-bindgen#environment-variables
          # LIBCLANG_PATH = pkgs.lib.makeLibraryPath [ pkgs.llvmPackages_latest.libclang.lib ];
          HISTFILE = toString ./.history;
          shellHook = ''
            export PATH=$PATH:~/.cargo/bin
            export PATH=$PATH:~/.rustup/toolchains/$RUSTC_VERSION-x86_64-unknown-linux-gnu/bin/
          '';
          # Add libvmi precompiled library to rustc search path
          # RUSTFLAGS = (builtins.map (a: ''-L ${a}/lib'') [
          #   pkgs.libvmi
          # ]);
          # Add libvmi, glibc, clang, glib headers to bindgen search path
          # BINDGEN_EXTRA_CLANG_ARGS =
          #   # Includes with normal include path
          #   (builtins.map (a: ''-I"${a}/include"'') [
          #     # pkgs.libvmi
          #     pkgs.glibc.dev
          #   ])
          #   # Includes with special directory paths
          #   ++ [
          #     ''-I"${pkgs.llvmPackages_latest.libclang.lib}/lib/clang/${pkgs.llvmPackages_latest.libclang.version}/include"''
          #     ''-I"${pkgs.glib.dev}/include/glib-2.0"''
          #     ''-I${pkgs.glib.out}/lib/glib-2.0/include/''
          #   ];

        };

        /* 

          libPath = with pkgs; lib.makeLibraryPath [
            libGL
            libxkbcommon
            wayland
            xorg.libX11
            xorg.libXcursor
            xorg.libXi
            xorg.libXrandr
          ];
        in
        pkgs.devshell.mkShell {
          imports = [ (pkgs.devshell.importTOML ./devshell.toml) ];
          packages = with pkgs; [
            rustc
            rust-analyzer
            cargo-edit
            trunk
            targets.wasm32-unknown-unknown.latest.rust-std

            # (fenix.packages."${system}".default.withComponents [
            #   "cargo"
            #   "clippy"
            #   "rustc"
            #   "rustfmt"
            # ])
            # pkgs.rust-analyzer-nightly 
          ];
          env = [
            { name = "RUST_SRC_PATH"; value = pkgs.rust.packages.stable.rustPlatform.rustLibSrc; }
            { name = "LD_LIBRARY_PATH"; value = libPath; }
          ];
          }; */
      });
}
