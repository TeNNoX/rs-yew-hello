use yew::prelude::*;

#[derive(Clone, Debug /* , Serialize, Deserialize */, PartialEq)]
pub struct Entry {
    pub title: String,
    pub children: Vec<Entry>,
}
impl Default for Entry {
    fn default() -> Self {
        Entry {
            title: String::from(""),
            children: Vec::new(),
        }
    }
}

#[derive(Properties, PartialEq)]
pub struct EntryDisplayProps {
    pub entry: Entry,
    pub add_child: Callback<()>,
}

#[function_component(EntryDisplay)]
pub fn entry_display(EntryDisplayProps { entry, add_child }: &EntryDisplayProps) -> Html {
    let counter = use_state(|| 0);
    let onclick = {
        // entry.children.push(Entry {title: String::from("Child"),children: Vec::new()});

        let counter = counter.clone();
        Callback::from(move |_| {
            // add_child.emit(());
            counter.set(*counter + 1)
        })
    };
    html! {
        <details>
            <summary style="cursor: pointer; display: flex; align-items: center; gap: 0.5rem;">
            {entry.title.clone()} <pre>{format!("{}, {}", entry.children.len(), *counter)}</pre>
            <button onclick={onclick}>{"Add Child"}</button>
            </summary>
            <div style="padding: 0.4rem; padding-left: 1rem; margin-bottom: 0.4rem;  border: 1px solid #ccc;">
                {entry.children.iter().flat_map(|n| std::iter::repeat(n).take(*counter)).map(|child| html! {
                        // <p>{child.title.clone()}</p>
                        <EntryDisplay entry={child.clone()} add_child={{ Callback::from(move |_| ()) }} />
                }).collect::<Html>()}

            </div>
        </details>
    }
}
