use yew::prelude::*;
mod list;
use list::entry::{Entry, EntryDisplay};

#[function_component(App)]
fn app() -> Html {
    let head = Entry {
        title: String::from("Test Head"),
        children: vec![Entry {
            title: "Child 1".into(),
            children: vec![Entry {
                title: "Child 2".into(),
                children: vec![],
            }],
        }],
    };
    let add_child = {
        Callback::from(move |_| {
            // head.children.push(Entry {
            //     title: String::from("Child 1"),
            //     children: vec![],
            // })
        })
    };
    html! {
        <>
            <h1>{ "Hello World" }</h1>
            <EntryDisplay entry={head} add_child={add_child} />
        </>
    }
}

fn main() {
    yew::start_app::<App>();
}
